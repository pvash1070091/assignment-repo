import {useState,useRef,useEffect} from 'react';
import './App.css';



function App() {

  
  const [time, setTime] = useState(3*60);
  const [start,setStart] = useState(false);
  const [initialV,setIni]  = useState(3*60);
 
  const Ref = useRef(null);
  const handleInput = (e) =>{
    if(e.target.value!=''){
    setTime(Math.floor(parseInt(e.target.value)*60))
    setIni(Math.floor(parseInt(e.target.value)*60))
    }
    else{
      if(start){
        clearInterval(start)
        setTime(180)
      }
    }
  }
  var timer;

  const startTimer = () =>{

  setStart(setInterval(run,1000))
  
 
  }
  var times = time;
  const run =() =>{
    if(times>0){
    times = times-1
    setTime(times);
    
    }
    else{
      clearInterval(start)
    }
  }
  const stopTime = () =>{
   
    clearInterval(start)
  }
  const continueTimer = () =>{
   
  
    setStart(setInterval(()=>{
              run();
      },1000))
    
   
    }
    const resetTimer = () =>{
      clearInterval(start);
      setTime(initialV)

    }

  return (
    <div className="App">
      <div>{time}</div>
      <div>
      <input type="text" onChange={handleInput}></input>
      </div>
      <div>
      <button id="start" onClick={startTimer}>Start</button>
      <button id="pause" onClick={stopTime}>Pause</button>
      <button id="continue" onClick={continueTimer}>continue</button>
      <button id="reset" onClick={resetTimer}>Reset</button>
      </div>
     
    </div>
  );
}

export default App;
